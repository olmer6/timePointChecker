class timePointChecker {
	public $startTimePoint;
	public $timePoints;
	
	public function __construct()
	{
		global $startTimePoint;
		if($startTimePoint)$this->startTimePoint = $startTimePoint;
		else $this->startTimePoint = microtime(true);		
		global $timePoints;
		if($timePoints)$this->timePoints = $timePoints;
		else $this->timePoints = array();	
	}
	
	public function addCheck($label=false)
	{
		$timepoint['time'] = microtime(true) - $this->startTimePoint;
		if($label)$timepoint['label'] = $label;
		$this->timePoints[] = $timepoint;
	}
	
	public function getArrCheck()
	{
		return $this->timePoints;
	}	
	
	public function arrCheckToLog()
	{		
		ob_start();
			var_dump(
				$this->timePoints
			); 
		$dump = ob_get_clean();
		
		file_put_contents($_SERVER["DOCUMENT_ROOT"].'/checerLog.txt', $dump."\r\n", FILE_APPEND);
	}
}
